import { Controller } from '@hotwired/stimulus';
export default class NavController extends Controller {
 static targets = ['navwrapper', 'navitem', 'elem']
 index;
 scrollDistance;
 scrollTarget;
 navWrapperTop;

 connect() {
  this.navWrapperTop = this.navitemTarget.offsetTop;
  this.navitemTargets.forEach((item) => {
   item.addEventListener("click", this.click);
  })
  this.navitemTargets[0].classList.add('nav__item-active');
  window.addEventListener('scroll', this.addClassWhenScroll);
  this.elemTargets.forEach((item, index) => {
   console.log(`${index + 1}: ${item.offsetTop}`)
  })
 }

 addClassWhenScroll = () => {
  this.scrollDistance = window.scrollY;
  this.elemTargets.forEach((item, index) => {
   if (item.offsetTop <= this.scrollDistance + this.navWrapperTop + 360) {
    this.navitemTargets.forEach((item) => {
     if (item.classList.contains('nav__item-active')) {
      item.classList.remove('nav__item-active');
     }
    })
    this.navitemTargets[index].classList.add('nav__item-active');
   }
  })
 }

 click = (e) => {
  e.preventDefault();
  this.index = this.navitemTargets.indexOf(e.target);
  const href = e.target.getAttribute("href").substring(1);
  this.scrollTarget = this.elemTargets.find(item => item.id == href);
  const elementPosition = this.scrollTarget.getBoundingClientRect().top;
  const offsetPosition = elementPosition - this.navitemTargets[this.index].getBoundingClientRect().top;
  window.scrollBy({
   top: offsetPosition,
   behavior: "smooth"
  })
 }
}