import { Controller } from '@hotwired/stimulus';
export default class AccordeonController extends Controller {
 static targets = ['placeholder', 'arrow', 'body'];

 check = false;
 height;

 connect() {
  this.getHeight();
  this.placeholderTarget.addEventListener('click', this.click);
 }

 click = () => {
  this.check = this.bodyTarget.classList.contains('accordeon-text-block');
  if (!this.check) {
   this.arrowTarget.classList.add('accordeon-arrow-top');
   this.placeholderTarget.classList.add('accordeon-placeholder-active');
   this.bodyTarget.classList.add('accordeon-text-block');
   this.bodyTarget.style.maxHeight = this.height;
  }
  else {
   this.bodyTarget.style.maxHeight = '0';
   this.arrowTarget.classList.remove('accordeon-arrow-top');
   this.placeholderTarget.classList.remove('accordeon-placeholder-active');
   this.bodyTarget.classList.remove('accordeon-text-block');
  }
 }

 getHeight = () => {
  this.height = getComputedStyle(this.bodyTarget).height;
  this.bodyTarget.style.maxHeight = '0';
 }
}