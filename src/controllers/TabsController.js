import { Controller } from '@hotwired/stimulus';
export default class TabsController extends Controller {
 static targets = ['button', 'tab', 'wrapper']
 height = [];

 connect() {
  this.buttonTarget.classList.add('typo__button-active')
  this.tabTarget.classList.add('typography__active')
  this.tabTargets.forEach((item) => {
   this.height.push(getComputedStyle(item).height)
  })
  this.wrapperTarget.style.minHeight = this.height[0];
 }

 switch = (e) => {
  this.buttonTargets.forEach((item, index) => {
   if (e.target == item) {
    this.tabTargets[index].classList.add('typography__active')
    item.classList.add('typo__button-active')
    this.wrapperTarget.style.minHeight = this.height[index];
   }
   else {
    this.tabTargets[index].classList.remove('typography__active')
    item.classList.remove('typo__button-active')
   }
  })
 }
}