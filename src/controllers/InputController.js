import { Controller } from '@hotwired/stimulus';
export default class InputController extends Controller {
 static targets = ['placeholder', 'input', 'error'];
 EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;
 inFocus = false;
 inputValue = '';
 
 connect() {
  this.inputTarget.addEventListener("keyup", this.showKeyUp);
  this.inputTarget.addEventListener("mouseenter", this.showPlaceholder);
  this.inputTarget.addEventListener("focus", this.showKeyUp);
  this.inputTarget.addEventListener("blur", this.blur);
  this.inputTarget.addEventListener("mouseout", this.mouseOut);
 }

 mouseOut = () => {
  if (this.inputValue == '' && !this.inFocus) {
   this.inputTarget.classList.remove('demonstration-input-hover', 'demonstration-input-typing');
  }
 }

 blur = () => {
  if (this.inputValue == '') {
   this.inFocus = false;
   this.placeholderTarget.classList.remove('demonstration-placeholder-active');
   this.inputTarget.classList.remove('demonstration-input-hover', 'demonstration-input-typing');
  }
 }

 showKeyUp = () => {
  this.inputValue = this.inputTarget.value;
  this.inFocus = true;
  this.inputTarget.classList.remove('demonstration-input-hover');
  this.inputTarget.classList.add('demonstration-input-typing');
  this.placeholderTarget.classList.add('demonstration-placeholder-active');
  if (!this.EMAIL_REGEXP.test(this.inputValue) && !this.inputValue == '') {
   this.placeholderTarget.classList.add('demonstration-placeholder-error');
   this.inputTarget.classList.add('demonstration-input-error');
   this.errorTarget.textContent = 'Введите корректный e-mail';
  }
  else {
   this.placeholderTarget.classList.remove('demonstration-placeholder-error');
   this.inputTarget.classList.remove('demonstration-input-error');
   this.errorTarget.textContent = '';
  }
 }

 showPlaceholder = () => {
  if (!this.inFocus) {
   this.inputTarget.classList.add('demonstration-input-hover');
  }
 }
}