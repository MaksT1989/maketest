import { Controller } from '@hotwired/stimulus';
export default class PopupController extends Controller {
 static targets = ['menu']
 showMenu() {
  this.menuTarget.classList.add('menu-active');
 }
 hideMenu() {
  this.menuTarget.classList.remove('menu-active');
 }
}