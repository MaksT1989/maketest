import { Controller } from '@hotwired/stimulus';
export default class DropdownController extends Controller {
 static targets = ['placeholder', 'choice', 'arrow', 'list'];
 changableChoice;

 connect() {
  this.placeholderTarget.addEventListener('click', this.placeholder);
 }

 placeholder = () => {
  this.placeholderTarget.classList.add('dropdown-placeholder-active');
  this.arrowTarget.classList.add('dropdown-arrow-top');
  this.listTarget.classList.add('dropdown-setofitems-active')
 }

 changeChoice = (e) => {
  if (this.changableChoice != e.target) {
   this.changableChoice = e.target;
   this.changableChoice.classList.add('dropdown-setofitems-item-blue');
   this.choiceTargets.forEach((item) => {
    if (this.changableChoice !== item) {
     item.classList.remove('dropdown-setofitems-item-blue');
    }
   })
  }
  this.placeholderTarget.classList.remove('dropdown-placeholder-active');
  this.placeholderTarget.innerText = e.target.innerText;
  this.arrowTarget.classList.remove('dropdown-arrow-top');
  this.listTarget.classList.remove('dropdown-setofitems-active')
 }
}