import { Application } from "@hotwired/stimulus";
import NavController from "./controllers/NavController";
import InputController from "./controllers/InputController";
import PopupController from "./controllers/PopupController";
import HeaderController from "./controllers/HeaderController";
import MenuController from "./controllers/MenuController";
import DropdownController from "./controllers/DropdownController";
import AccordeonController from "./controllers/AccordeonController";
import TabsController from "./controllers/TabsController";


const application = Application.start();
application.register('popup', PopupController);
application.register('header', HeaderController);
application.register('input', InputController);
application.register('menu', MenuController);
application.register('dropdown', DropdownController);
application.register('accordeon', AccordeonController);
application.register('tabs', TabsController);
application.register('nav', NavController);
